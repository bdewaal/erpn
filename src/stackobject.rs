use std::fmt;
use units::CompositeUnit;

/// An object that lives on the stack
#[derive(Clone, PartialEq)]
pub struct StackObject {
    pub value: f64,
    pub unit: CompositeUnit,
}

impl fmt::Display for StackObject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.value, self.unit.short_name())
    }
}

impl StackObject {
    pub fn new(value: f64, unit: CompositeUnit) -> Self {
        Self { value, unit }
    }

    pub fn sum(&self, other: &Self) -> Option<Self> {
        Some(Self::new(
            self.value + other.unit.scale_value_to(other.value, &self.unit)?,
            self.unit.clone(),
        ))
    }

    pub fn sub(&self, other: &Self) -> Option<Self> {
        Some(Self::new(
            self.value - other.unit.scale_value_to(other.value, &self.unit)?,
            self.unit.clone(),
        ))
    }

    pub fn mul(&self, other: &Self) -> Option<Self> {
        Some(Self::new(
            self.value * other.value,
            self.unit.mul(&other.unit),
        ))
    }

    pub fn div(&self, other: &Self) -> Option<Self> {
        if other.value != 0.0 {
            Some(Self::new(
                self.value / other.value,
                self.unit.div(&other.unit),
            ))
        } else {
            None
        }
    }

    pub fn convert_to_unit(&self, unit: &CompositeUnit) -> Option<Self> {
        self.unit
            .scale_value_to(self.value, unit)
            .map(|value| Self::new(value, unit.clone()))
    }
}
