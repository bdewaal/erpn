use stackmanager::CalculatorFunction as CF;
use stackobject::StackObject;
use undoobject::UndoObject;
use units::CompositeUnit;

pub struct Stack {
    pub values: Vec<StackObject>,
    pub undo: Vec<UndoObject>,
    pub redo: Vec<CF>,
}

/// Setup a x,x -> x function for use in the big match below
/// Helps with returning the state to the previous if there is an error
macro_rules! stack_function {
    ($self:ident, $fun_name:ident, $failure_text:tt, $action:expr) => {
        if $self.values.len() >= 2 {
            let first = $self.values.pop().unwrap();
            let second = $self.values.pop().unwrap();
            if let Some(val) = second.$fun_name(&first) {
                $self.values.push(val);
                $self
                    .undo
                    .push(UndoObject::new(vec![second, first], 1, $action));
                None
            } else {
                $self.values.push(second);
                $self.values.push(first);
                Some($failure_text.to_string())
            }
        } else {
            Some("Not enough values".to_string())
        }
    };
}

/// A full stack of objects, wich the user would want to do operations on
impl Stack {
    /// Create a new stack
    pub fn new() -> Self {
        Self {
            values: vec![],
            undo: vec![],
            redo: vec![],
        }
    }

    /// Output a vector of strings, representing each item in the stack
    /// The first item in the vector is the top item on the stack
    pub fn to_strings(&self) -> Vec<String> {
        let mut out = Vec::new();
        for val in self.values.iter() {
            out.push(format!("{}", val));
        }
        out
    }

    pub fn run_action(&mut self, action: &CF) -> Option<String> {
        let val = self.run_action_internal(action);
        if *action != CF::Undo && *action != CF::Redo {
            self.redo.clear();
        }
        val
    }

    fn run_action_internal(&mut self, action: &CF) -> Option<String> {
        match action {
            CF::Add => stack_function!(self, sum, "Could not add", action.clone()),
            CF::Subtract => stack_function!(self, sub, "Could not subtract", action.clone()),
            CF::Multiply => stack_function!(self, mul, "Could not multiply", action.clone()),
            CF::Divide => stack_function!(self, div, "Could not divide", action.clone()),
            CF::Remove => self.remove(),
            CF::Cycle(x) => self.cycle_n(*x),
            CF::Undo => self.undo(),
            CF::Redo => {
                if let Some(x) = self.redo.pop() {
                    self.run_action_internal(&x)
                } else {
                    Some("Nothing to redo".to_string())
                }
            }
            CF::ApplyUnit(x) => self.apply_unit(x),
            CF::ConvertUnit(x) => self.convert_to_unit(x),
            CF::AddStackObject(x) => {
                self.values.push(x.clone());
                self.undo.push(UndoObject::new(
                    Vec::new(),
                    1,
                    CF::AddStackObject(x.clone()),
                ));
                None
            }
            CF::SetMode(_) => panic!("SetMode should never be passed to stack"),
        }
    }

    /// Remove the bottom item from the stack
    fn remove(&mut self) -> Option<String> {
        if let Some(x) = self.values.pop() {
            self.undo.push(UndoObject::new(vec![x], 0, CF::Remove));
            None
        } else {
            Some("Nothing to delete".to_string())
        }
    }

    /// Cycle through the bottom n items in the stack
    fn cycle_n(&mut self, n: u8) -> Option<String> {
        let mut original; // Items cycled in original order for undo
        {
            // Scope so we can use n as usize
            let len = self.values.len();
            let n = n as usize;
            if len < n {
                return Some("Not enough values".to_string());
            }

            // Copy the items in the original order, for undo
            original = Vec::new();
            for i in 0..n as usize {
                original.push(self.values[len - n + i].clone())
            }

            let x = self.values.remove(len - n);
            self.values.push(x);
        }

        self.undo.push(UndoObject::new(original, n, CF::Cycle(n)));

        None
    }

    /// Multiply the current stack object by the unit
    /// This is how a unit is usually applied:
    /// If we have 1.0, and we want 1.0 m, we multiply 1.0 by 'm'
    fn apply_unit(&mut self, unit: &CompositeUnit) -> Option<String> {
        let new_unit_val = StackObject::new(1.0, unit.clone());

        // Find what the new value should be
        let mut new_val;
        let undo_item;
        if let Some(old_val) = self.values.pop() {
            if let Some(mult) = old_val.mul(&new_unit_val) {
                new_val = mult;
                undo_item = UndoObject::new(vec![old_val], 1, CF::ApplyUnit(unit.clone()));
            } else {
                self.values.push(old_val);
                return Some("Could not apply unit".to_string());
            }
        } else {
            new_val = new_unit_val;
            undo_item = UndoObject::new(Vec::new(), 1, CF::ApplyUnit(unit.clone()));
        }

        // Clean the units up a little
        new_val.unit.sweep();

        // And push to the stacks
        self.values.push(new_val);
        self.undo.push(undo_item);

        None
    }

    /// Convert the bottom item on the stack to the supplied unit
    /// It already needs to have the right dimension, we only change the unit
    /// So we can use it to convert "1.0 m" to ft, but not "1.0" to ft.
    /// Use "apply_unit" for that.
    pub fn convert_to_unit(&mut self, unit: &CompositeUnit) -> Option<String> {
        if let Some(x) = self.values.pop() {
            if let Some(new_x) = x.convert_to_unit(unit) {
                self.values.push(new_x);
                None
            } else {
                self.values.push(x);
                Some("Could not convert unit".to_string())
            }
        } else {
            Some("Nothing on the stack".to_string())
        }
    }

    /// Take the top item off the undo stack, and use it to undo
    pub fn undo(&mut self) -> Option<String> {
        if let Some(mut u) = self.undo.pop() {
            for _ in 0..u.added_items_count {
                self.values.pop();
            }
            self.values.append(&mut u.removed_items);
            self.redo.push(u.action);
            None
        } else {
            Some("Nothing to undo".to_string())
        }
    }
}
