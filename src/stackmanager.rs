use actionsmap::{gen_actions_map, ActionsMap, CalculatorFunctionHolder};
use messages::{Key, MessageToGui, MessageToStack};
use stack::Stack;
use stackobject::StackObject;
use std::sync::mpsc::{Receiver, Sender};
use units::CompositeUnit;
use MessageToStack::{SendEntry, SendKey};

/// In the different modes the keybindings are different
#[derive(Copy, Clone, Eq, Hash, PartialEq)]
pub enum UIMode {
    NormalMode,
    UnitsMode,
}

/// An actual function the calculator can execute
/// Every function the calculator has will be matched from this enum
#[derive(Clone, PartialEq)]
pub enum CalculatorFunction {
    Add,
    Subtract,
    Multiply,
    Divide,
    Cycle(u8),
    Remove,
    ApplyUnit(CompositeUnit),
    ConvertUnit(CompositeUnit),
    SetMode(UIMode),
    Undo,
    Redo,
    AddStackObject(StackObject),
}

/// Tread to manage a stack.
/// Will handle commands that come through to_stack, and will send relevant messages to to_gui
pub fn start_stack(to_stack: Receiver<MessageToStack>, to_gui: Sender<MessageToGui>) {
    let mut stack = Stack::new();
    let mut mode = UIMode::NormalMode;
    let actions_map = gen_actions_map();

    set_ui_text(&to_gui, &mode, &actions_map);

    loop {
        match to_stack.recv() {
            Ok(SendKey(key, ctrl, alt)) => {
                handle_key(&to_gui, &mut stack, &actions_map, &mut mode, key, ctrl, alt);
            }
            // Receiving a new value from the Gui
            Ok(SendEntry(val_string)) => {
                match val_string.parse::<f64>() {
                    // Attempt to parse
                    Ok(val) => {
                        stack.run_action(&CalculatorFunction::AddStackObject(StackObject::new(
                            val,
                            CompositeUnit::new_empty(),
                        )));
                        to_gui
                            .send(MessageToGui::ShowStack(stack.to_strings()))
                            .unwrap();
                        to_gui.send(MessageToGui::ClearError).unwrap();
                    }
                    _ => to_gui
                        .send(MessageToGui::ShowError("Could not parse".to_string()))
                        .unwrap(),
                }
            }
            Err(_) => (),
        }
    }
}

/// Perform the action required when key is received
// Right now the keymapping is hard-coded, the plan is to have a config file at some point.
fn handle_key(
    to_gui: &Sender<MessageToGui>,
    stack: &mut Stack,
    actions_map: &ActionsMap,
    mode: &mut UIMode,
    key: Key,
    ctrl: bool,
    alt: bool,
) {
    match actions_map.get(&(*mode, key.clone(), ctrl, alt)) {
        Some(f) => match &f.function {
            CalculatorFunction::SetMode(x) => {
                *mode = *x;
                set_ui_text(to_gui, mode, actions_map);
            }
            x => {
                if let Some(err) = stack.run_action(x) {
                    to_gui.send(MessageToGui::ShowError(err)).unwrap();
                } else {
                    to_gui
                        .send(MessageToGui::ShowStack(stack.to_strings()))
                        .unwrap();
                    to_gui.send(MessageToGui::ClearError).unwrap();
                }
            }
        },
        None => {
            eprintln!("key {}, ctrl:{} alt:{}", key, ctrl, alt);
        }
    }
}

/// Output the text to show in the sidebar
fn set_ui_text(ui: &Sender<MessageToGui>, mode: &UIMode, actions_map: &ActionsMap) {
    let mut msgs = Vec::new();
    msgs.push("This is an RPN Calculator!".to_string());

    match mode {
        UIMode::NormalMode => {
            msgs.push("Normal Mode".to_string());
        }
        UIMode::UnitsMode => {
            msgs.push("Units Mode".to_string());
        }
    }

    let mut actions: Vec<&CalculatorFunctionHolder> = actions_map
        .iter()
        .filter_map(|(key, value)| {
            if key.0 == *mode && !value.hidden {
                Some(value)
            } else {
                None
            }
        })
        .collect();
    actions.sort_by_key(|x| x.order);
    for i in actions.iter() {
        msgs.push(format!("{}: {}", i.key_name, i.name));
    }
    let _ = ui.send(MessageToGui::ShowHelp(msgs));
}
