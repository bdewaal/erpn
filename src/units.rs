use std::cmp::max;
use std::collections::HashMap;
use std::mem;

#[derive(Clone, Debug, PartialEq)]
/// A unit of measurement
/// Represented by a point in the unit space (for example, accelleration is in m^1 s^-2, so a unit
/// for acceleration could be [1, -2]).
/// The scale is the number you need to multiply the base with to get this unit. So ft would have a
/// scale of 3.28, because you need to multiply m with 3.28 to get ft.
/// The long name needs to be unique for the unit space vector, as it will be hashed on
pub struct Unit {
    short_name: String, // For example, SI abbrevition
    long_name: String,
    /// Every base unit (e.g. meter) is assigned a dimension. When we combine them, we then have a
    /// unique way of representing the thing that is expressed by that unit. That way we can check
    /// that e.g. m/s and mile/hr represents the same measurement with a different scale.
    unit_space_vector: Vec<i16>,
    scale: f64,
    si_prefix: bool,
}

impl Unit {
    /// Create new base unit (normally only used for base SI units)
    /// All other units will be combinations of these units
    fn new_base_unit(short: &str, long: &str, si_prefix: bool, usv: Vec<i16>) -> Self {
        Self {
            short_name: short.to_string(),
            long_name: long.to_string(),
            unit_space_vector: usv,
            scale: 1.0,
            si_prefix,
        }
    }

    pub fn new_unit(
        short: &str,
        long: &str,
        si_prefix: bool,
        base_on: &CompositeUnit,
        scale: f64,
    ) -> Self {
        Self {
            short_name: short.to_string(),
            long_name: long.to_string(),
            unit_space_vector: base_on.unit_space_vector(),
            scale: scale * base_on.scale(),
            si_prefix,
        }
    }

    /// Return the size in the nth unit dimension
    fn dimension(&self, n: usize) -> i16 {
        *self.unit_space_vector.get(n).unwrap_or(&0)
    }

    pub fn as_composite_unit(&self) -> CompositeUnit {
        CompositeUnit {
            units: vec![(1, self.clone())],
        }
    }
}

/// A series of units multiplied with each other.
/// When an object on the stack has a unit, this is what is meant.
#[derive(Clone, Debug, PartialEq)]
pub struct CompositeUnit {
    units: Vec<(i16, Unit)>,
}

impl CompositeUnit {
    /// Return the size in the nth unit dimension
    fn dimension(&self, n: usize) -> i16 {
        self.units.iter().map(|x| x.0 * x.1.dimension(n)).sum()
    }

    /// The length you need to check for dimensions in this unit
    /// Note: can return 0 for empty sets
    fn dimension_length(&self) -> usize {
        self.units
            .iter()
            .map(|x| x.1.unit_space_vector.len())
            .max()
            .unwrap_or(0)
    }

    /// Check if two composite units describe the same quality
    fn same_dimension_as(&self, other: &Self) -> bool {
        (0..max(self.dimension_length(), other.dimension_length()))
            .all(|n| self.dimension(n) == other.dimension(n))
    }

    /// Return the vector of dimensions as based on the basic units
    fn unit_space_vector(&self) -> Vec<i16> {
        let mut result = Vec::new();
        for i in 0..self.dimension_length() {
            result.push(self.dimension(i));
        }
        result
    }

    fn scale(&self) -> f64 {
        self.units
            .iter()
            .map(|x| x.1.scale.powi(x.0 as i32))
            .product()
    }

    pub fn scale_value_to(&self, value: f64, to_unit: &Self) -> Option<f64> {
        if self.same_dimension_as(to_unit) {
            Some(value / self.scale() * to_unit.scale())
        } else {
            None
        }
    }

    pub fn new_empty() -> Self {
        Self { units: vec![] }
    }

    fn dedup(&mut self) {}

    pub fn mul(&self, other: &Self) -> Self {
        let mut new_comp = (*self).clone();
        for u in other.units.iter() {
            new_comp.units.push(u.clone());
        }
        new_comp.dedup();
        new_comp
    }

    /// Create the division of two units
    pub fn div(&self, other: &Self) -> Self {
        let mut new_comp = (*self).clone();
        for u in other.units.iter() {
            new_comp.units.push((-u.0, u.1.clone()));
        }
        new_comp.dedup();
        new_comp
    }

    pub fn short_name(&self) -> String {
        let mut sn = String::new();
        for u in self.units.iter() {
            if u.0 == 1 {
                sn.push_str(&format!("{} ", u.1.short_name));
            } else {
                sn.push_str(&format!("{}^{} ", u.1.short_name, u.0));
            }
        }
        sn
    }

    pub fn name(&self) -> String {
        if self.units.len() == 1 && self.units[0].0 == 1 {
            return self.units[0].1.long_name.clone();
        }
        let mut sn = String::new();
        for u in self.units.iter() {
            if u.0 == 1 {
                sn.push_str(&format!("{} ", u.1.short_name));
            } else {
                sn.push_str(&format!("{}^{} ", u.1.short_name, u.0));
            }
        }
        sn
    }

    /// Make sure that every unit is only listed once
    pub fn sweep(&mut self) {
        let mut final_unit = Self::new_empty();
        loop {
            if !self.units.is_empty() {
                let (mut next_count, next_unit) = self.units.remove(0);

                // drain filter is still nigthly only, so do it by hand
                let mut i = 0;
                while i != self.units.len() {
                    // We compare units on long name and unit space vector
                    if next_unit.long_name == self.units[i].1.long_name
                        && next_unit.unit_space_vector == self.units[i].1.unit_space_vector
                    {
                        // If it has the same unit again, just add to the previous
                        let (more_num, _) = self.units.remove(i);
                        next_count += more_num;
                    } else {
                        i += 1;
                    }
                }
                if next_count != 0 {
                    final_unit.units.push((next_count, next_unit));
                }
            } else {
                break;
            }
        }
        mem::swap(&mut final_unit, self);
    }

    pub fn invert(&mut self) {
        for u in self.units.iter_mut() {
            u.0 = -u.0;
        }
    }
    pub fn inverted(&self) -> Self {
        let mut x = self.clone();
        x.invert();
        x
    }
}

/// Struct to hold all the defined units (with a name)
/// Including any lookup functions to retrieve them.
#[derive(Debug)]
pub struct UnitHolder {
    units: HashMap<String, Unit>,
    /// keep track of how many dimensions we need for the unit space vector
    base_unit_count: usize,
}

impl UnitHolder {
    /// Initiate new `UnitHolder`
    pub fn new() -> Self {
        Self {
            units: HashMap::new(),
            base_unit_count: 0,
        }
    }

    /// Create new base unit
    /// Arguments:
    ///  - `short` name (e.g. "m")
    ///  - `long` name (e.g. "meter")
    ///  - `si_prefix`: true if we can use prefixes like m, M, k with this unit
    ///  Returns the composite unit in case it's ever used
    pub fn new_base_unit(&mut self, short: &str, long: &str, si_prefix: bool) -> CompositeUnit {
        self.base_unit_count += 1;
        let mut usv: Vec<i16> = (0..self.base_unit_count).map(|_| 0).collect(); // New unit space vector
        usv[self.base_unit_count - 1] = 1;

        let new_unit = Unit::new_base_unit(short, long, si_prefix, usv);
        let ret = new_unit.as_composite_unit();
        self.units.insert(long.to_string(), new_unit);
        ret
    }

    /// Create new unit based on other units
    /// Arguments:
    ///  - `short` name (e.g. "ft")
    ///  - `long` name (e.g. "foot")
    ///  - `si_prefix`: true if we can use prefixes like m, M, k with this unit
    ///  - `base_on`: `CompositeUnit` to base the dimensionality on
    ///  - `scale`: the number to multiply with to convert to the base_on unit.
    ///    e.g. from ft to meter would be 3.28084
    ///  Returns the `CompositeUnit` derived from this unit
    pub fn new_unit(
        &mut self,
        short: &str,
        long: &str,
        si_prefix: bool,
        base_on: &CompositeUnit,
        scale: f64,
    ) {
        let new = Unit::new_unit(short, long, si_prefix, base_on, scale);
        self.units.insert(long.to_string(), new);
        println!("{:?}", self.units);
    }

    /// Find the unit according to the long name
    /// Arguments:
    /// - `key`: the long name to look for
    /// Returns the unit as a `CompositeUnit`.
    pub fn find(&self, key: &str) -> Option<CompositeUnit> {
        self.units.get(key).map(|unit| unit.as_composite_unit())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn units_maker() -> Vec<CompositeUnit> {
        let mut holder = UnitHolder::new();
        let meter = holder.new_base_unit("m", "meter", true);
        let second = holder.new_base_unit("s", "second", true);
        vec![meter, second]
    }

    #[test]
    fn divide() {
        let units = units_maker();
        let meter_per_second = units[0].div(&units[1]);
        assert_eq!(meter_per_second.dimension(0), 1);
        assert_eq!(meter_per_second.dimension(1), -1);
        assert_eq!(meter_per_second.dimension(2), 0);
    }

    #[test]
    fn multiply() {
        let units = units_maker();
        let meter_per_second = units[0].mul(&units[1]);
        assert_eq!(meter_per_second.dimension(0), 1);
        assert_eq!(meter_per_second.dimension(1), 1);
        assert_eq!(meter_per_second.dimension(2), 0);
    }

    #[test]
    fn non_base_unit_dimensions() {
        let units = units_maker();
        let meter = &(units[0].units[0].1);
        let second = &(units[1].units[0].1);
        let ft = Unit::new_unit("ft", "foot", false, &meter.as_composite_unit(), 3.28084);
        let min = Unit::new_unit(
            "min",
            "minute",
            false,
            &second.as_composite_unit(),
            1.0 / 60.0,
        );

        let ft_per_minute = ft.as_composite_unit().div(&min.as_composite_unit());
        assert_eq!(ft_per_minute.dimension(0), 1);
        assert_eq!(ft_per_minute.dimension(1), -1);
        assert_eq!(ft_per_minute.dimension(2), 0);
    }

    #[test]
    fn scale_conversion() {
        let units = units_maker();
        let meter_per_second = units[0].div(&units[1]);

        let meter = &(units[0].units[0].1);
        let second = &(units[1].units[0].1);
        let ft = Unit::new_unit("ft", "foot", false, &meter.as_composite_unit(), 3.28084);
        let min = Unit::new_unit(
            "min",
            "minute",
            false,
            &second.as_composite_unit(),
            1.0 / 60.0,
        );

        let ft_per_minute = ft.as_composite_unit().div(&min.as_composite_unit());

        // As this is floating point we won't get an exact value, but it should be small
        let one_ft_per_min = meter_per_second
            .scale_value_to(1.0, &ft_per_minute)
            .unwrap();
        assert!((one_ft_per_min - 196.9).abs() < 0.1);
    }

    #[test]
    fn scale_conversion_squared() {
        let units = units_maker();
        let meter = &(units[0].units[0].1);
        let ft = Unit::new_unit("ft", "foot", false, &meter.as_composite_unit(), 3.28084);
        let ft_c = ft.as_composite_unit();
        let inch = Unit::new_unit("inch", "inch", false, &ft_c, 12.00);
        let inch_c = inch.as_composite_unit();

        // Six inches long, but smells like a foot
        let six_inches = inch_c.scale_value_to(6.0, &ft_c).unwrap();
        assert!((six_inches - 0.5).abs() < 0.001);

        // Now we try square inches and square feet
        let mut sq_ft = ft_c.mul(&ft_c);
        let mut sq_inch = inch_c.mul(&inch_c);

        let sq_ft_to_inch = sq_ft.scale_value_to(1.0, &sq_inch).unwrap();
        assert_eq!(sq_ft_to_inch, 144.0);

        // but does it still work after sweep()?
        sq_ft.sweep();
        sq_inch.sweep();

        let sq_ft_to_inch = sq_ft.scale_value_to(1.0, &sq_inch).unwrap();
        assert_eq!(sq_ft_to_inch, 144.0);
    }

    #[test]
    /// We can build a unit that is not the same quantity as the base unit, and use that to convert
    /// In this test we define a unit "c", as in fraction of speed of light
    fn unit_based_on_composite_scale() {
        let units = units_maker();
        let meter_per_second = units[0].div(&units[1]);
        let c = Unit::new_unit("c", "speed of light", false, &meter_per_second, 1.0 / 3e8);

        let half_c = meter_per_second
            .scale_value_to(1.5e8, &c.as_composite_unit())
            .unwrap();
        assert!((half_c - 0.5).abs() < 0.001);
    }
}
