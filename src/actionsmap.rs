use json;
use messages::{keys, Key};
use stackmanager::CalculatorFunction::*;
use stackmanager::{CalculatorFunction, UIMode};
use std::collections::HashMap;
use units::{Unit, UnitHolder};

pub type ActionsMap = HashMap<(UIMode, Key, bool, bool), CalculatorFunctionHolder>;

/// Holder for a calculator function
/// Everything besides the function itself is used for the in-program documentation
pub struct CalculatorFunctionHolder {
    pub function: CalculatorFunction,
    pub name: String,     // The name as displayed to the user
    pub key_name: String, // Printable display of keys to press to display this
    pub hidden: bool, // Whether this function should show up in documentation. Useful for functions mapped to two keys
    pub order: u16,   // In what order to display these items, lower numbers are displayed first
}

/// Macro for creating CalculatorFunctionHolder objects
macro_rules! hold_function {
    ($function:expr, $name:expr, $key_name:expr, $hidden:expr, $order:expr) => {
        CalculatorFunctionHolder {
            function: $function,
            name: $name.to_string(),
            key_name: $key_name.to_string(),
            hidden: $hidden,
            order: $order,
        }
    };
    ($function:expr, $name:expr, $key_name:expr, $order:expr) => {
        hold_function!($function, $name, $key_name, false, $order)
    };
    (~$function:expr, $_key_name:expr) => {
        hold_function!($function, "", "", true, 0)
    };
    (~$function:expr) => {
        hold_function!($function, "", "", true, 0)
    };
}

/// Macro to generate the keypress tuples to match against
/// First letter is mode (N for normal or U for Units)
/// ^ stands for control, a for alt key
/// Tuple is (mode, key, ctrl, alt)
/// ```
/// assert_eq(k!(N 97), (UIMode::NormalMode, 'a' as u32, false, false));
/// assert_eq(k!(U ^100), (UIMode::NormalMode, 'd' as u32, true, false));
/// ```
macro_rules! k {
    (N $key:expr) => {
        (UIMode::NormalMode, $key, false, false)
    };
    (N ^$key:expr) => {
        (UIMode::NormalMode, $key, true, false)
    };
    (N a $key:expr) => {
        (UIMode::NormalMode, $key, false, true)
    };
    (N ^a $key:expr) => {
        (UIMode::NormalMode, $key, true, true)
    };
    (U $key:expr) => {
        (UIMode::UnitsMode, $key, false, false)
    };
    (U ^$key:expr) => {
        (UIMode::UnitsMode, $key, true, false)
    };
    (U a $key:expr) => {
        (UIMode::UnitsMode, $key, false, true)
    };
    (U ^a $key:expr) => {
        (UIMode::UnitsMode, $key, true, true)
    };
}

/// Create the mappings for a unit
/// This is the letter for the map
/// ^letter for inverse
/// and alt-^ for convert
/// For now only supporting adding to the Units menu
macro_rules! unit_map {
    (U $map:ident, $key:expr, $unit:expr, $order:expr) => {
        $map.insert(k!(U $key), hold_function!(ApplyUnit($unit), $unit.name(), $key.to_unicode().unwrap(), $order));
        $map.insert(k!(U ^$key), hold_function!(~ ApplyUnit($unit.inverted())));
        $map.insert(k!(U a $key), hold_function!(~ ConvertUnit($unit)));
        $map.insert(k!(U ^a $key), hold_function!(~ ConvertUnit($unit.inverted())));
    };
}

pub fn gen_actions_map() -> ActionsMap {
    let mut actions_map = HashMap::new();
    actions_map.insert(k!(N keys::plus), hold_function!(Add, "add", "+ a", 10));
    actions_map.insert(k!(N keys::a), hold_function!(~Add));
    actions_map.insert(k!(N keys::KP_Add), hold_function!(~Add));

    actions_map.insert(
        k!(N keys::minus),
        hold_function!(Subtract, "subtract", "- s", 15),
    );
    actions_map.insert(k!(N keys::KP_Subtract), hold_function!(~ Subtract));
    actions_map.insert(k!(N keys::s), hold_function!(~ Subtract));

    actions_map.insert(
        k!(N keys::m),
        hold_function!(Multiply, "multiply", "* m", 20),
    );
    actions_map.insert(k!(N keys::asterisk), hold_function!(~ Multiply));
    actions_map.insert(k!(N keys::KP_Multiply), hold_function!(~ Multiply));

    actions_map.insert(
        k!(N keys::slash),
        hold_function!(Divide, "divide", "/ d", 25),
    );
    actions_map.insert(k!(N keys::d), hold_function!(~ Divide));
    actions_map.insert(k!(N keys::KP_Divide), hold_function!(~ Divide));

    actions_map.insert(k!(N keys::c), hold_function!(Cycle(3), "cycle 3", "c", 40));
    actions_map.insert(
        k!(N keys::Tab),
        hold_function!(Cycle(2), "swap", "<tab>", 39),
    );

    actions_map.insert(
        k!(N keys::Delete),
        hold_function!(Remove, "remove", "x <del>", 50),
    );
    actions_map.insert(k!(N keys::x), hold_function!(~ Remove));
    actions_map.insert(k!(N keys::KP_Delete), hold_function!(~ Remove));

    actions_map.insert(k!(N keys::u), hold_function!(Undo, "undo", "u", 990));
    actions_map.insert(k!(N keys::r), hold_function!(Redo, "redo", "r", 991));

    actions_map.insert(
        k!(N keys::U),
        hold_function!(SetMode(UIMode::UnitsMode), "Units", "U", 1000),
    );

    actions_map.insert(k!(U keys::c), hold_function!(Cycle(3), "cycle 3", "c", 40));
    actions_map.insert(
        k!(U keys::Tab),
        hold_function!(Cycle(2), "swap", "<tab>", 39),
    );
    actions_map.insert(
        k!(U keys::U),
        hold_function!(SetMode(UIMode::NormalMode), "Normal Mode", "U", 1000),
    );

    // Units
    let mut unit_holder = UnitHolder::new();

    let _ = load_config(&mut unit_holder);

    unit_map!(U actions_map, keys::m, unit_holder.find("meter").unwrap(), 100);
    unit_map!(U actions_map, keys::s, unit_holder.find("second").unwrap(), 200);

    actions_map
}

fn load_config(unit_holder: &mut UnitHolder) -> std::io::Result<()> {
    let f = std::fs::read_to_string("config.json")?;
    let config = json::parse(&f).unwrap();

    println!("Add base units");
    for base_unit in config["base_units"].members() {
        let name_opt = base_unit["name"].as_str();
        let short_opt = base_unit["short"].as_str();
        if let (Some(name), Some(short)) = (name_opt, short_opt) {
            unit_holder.new_base_unit(short, name, true);
        } else {
            println!(
                "Error parsing config, unit {}: can't parse arguments. Skipped unit",
                base_unit
            );
        }
    }
    println!("Base unit dict: {:?}", unit_holder);

    println!("Add derived units");
    for x in config["derived_units"].members() {
        println!("{}", x);
    }

    println!("Add menus");
    for (x, y) in config["menus"].entries() {
        println!("  entry {}", x);
        for key in y.members() {
            println!(" --> {}", key);
        }
    }

    Ok(())
}
