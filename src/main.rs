extern crate gtk;
use gtk::prelude::*;
use gtk::{
    Align, Box, Entry, Inhibit, Label, Orientation, PackType, Separator, Window, WindowType,
};
extern crate gdk;
use gdk::{EventKey, ModifierType};
extern crate glib;
extern crate json;

mod messages;
use messages::keys;
use messages::MessageToGui::{ClearError, ShowError, ShowHelp, ShowStack};
use messages::MessageToStack;
use std::sync::mpsc::channel;
use std::thread;

mod actionsmap;
mod stack;
mod stackmanager;
mod stackobject;
mod undoobject;
mod units;

use stackmanager::start_stack;

fn main() {
    if gtk::init().is_err() {
        println!("Failed to init GTK");
        return;
    }

    let (gui_sender, gui_receiver) = channel();
    let (stack_sender, stack_receiver) = channel();
    thread::spawn(|| start_stack(stack_receiver, gui_sender));

    let window = Window::new(WindowType::Toplevel);
    window.set_title("Eristic RPN Calculator");
    window.set_default_size(800, 580);

    let vertical_gui_objects = Box::new(gtk::Orientation::Vertical, 10);
    let left_right_split = Box::new(gtk::Orientation::Horizontal, 10);
    window.add(&vertical_gui_objects);
    vertical_gui_objects.add(&left_right_split);
    vertical_gui_objects.set_property_margin(10);
    vertical_gui_objects.set_child_packing(&left_right_split, true, true, 0, PackType::Start);

    let stack_view = Label::new(None);
    stack_view.set_markup("<tt></tt>");
    stack_view.set_halign(Align::Start);
    stack_view.set_valign(Align::End);
    left_right_split.add(&stack_view);
    left_right_split.set_child_packing(&stack_view, true, true, 0, PackType::Start);

    let v_divider = Separator::new(Orientation::Vertical);
    left_right_split.add(&v_divider);

    let help_view = Label::new(None);
    help_view.set_halign(Align::Start);
    help_view.set_valign(Align::Start);
    left_right_split.add(&help_view);

    let h_divider = Separator::new(Orientation::Horizontal);
    vertical_gui_objects.add(&h_divider);

    let message_view = Label::new(None);
    message_view.set_halign(Align::Start);
    vertical_gui_objects.add(&message_view);

    let input_box = Entry::new();
    vertical_gui_objects.add(&input_box);

    window.show_all();

    // This has to be set after show_all, otherwise the text starts of selected
    stack_view.set_selectable(true);

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    window.connect_key_press_event(move |_me: &Window, key: &EventKey| {
        // We handle all keypresses here
        let keyval = key.get_keyval();
        let ctrl = key.get_state().intersects(ModifierType::CONTROL_MASK);
        let alt = key.get_state().intersects(ModifierType::MOD1_MASK);
        match keyval {
            keys::decimalpoint
            | keys::KP_Decimal
            | keys::_0
            | keys::KP_0
            | keys::_1
            | keys::KP_1
            | keys::_2
            | keys::KP_2
            | keys::_3
            | keys::KP_3
            | keys::_4
            | keys::KP_4
            | keys::_5
            | keys::KP_5
            | keys::_6
            | keys::KP_6
            | keys::_7
            | keys::KP_7
            | keys::_8
            | keys::KP_8
            | keys::_9
            | keys::KP_9
            | keys::period => {
                // Digits or decimal
                let mut current_text = input_box.get_text().to_string();
                current_text.push(keyval.to_unicode().unwrap());
                input_box.set_text(&current_text);
            }
            x => {
                // First we send the number in the entry box
                let txt = input_box.get_text().to_string(); // get_text returns a "gstring", but we like normal strings
                input_box.set_text("");
                if !txt.is_empty() {
                    stack_sender.send(MessageToStack::SendEntry(txt)).unwrap();
                    if x != keys::Return && x != keys::KP_Enter {
                        // If we just entered a new entry, we don't want to send enter through
                        stack_sender
                            .send(MessageToStack::SendKey(x, ctrl, alt))
                            .unwrap();
                    }
                } else {
                    stack_sender
                        .send(MessageToStack::SendKey(x, ctrl, alt))
                        .unwrap();
                }
            }
        }
        Inhibit(true)
    });

    // This is the function that receives commands from the stack
    glib::idle_add_local(move || {
        match gui_receiver.try_recv() {
            Ok(ShowError(err)) => {
                message_view.set_markup(&format!(
                    "<span foreground=\"#FF0000\"><i>{}</i></span>",
                    &err
                ));
            }
            Ok(ClearError) => {
                message_view.set_markup("");
            }
            Ok(ShowStack(stack_items)) => {
                let out = stack_items.join("\n");
                stack_view.set_markup(&format!("<tt>{}</tt>", out));
            }
            Ok(ShowHelp(help_lines)) => {
                let mut out = String::new();
                for l in help_lines.iter() {
                    out.push_str(l);
                    out.push('\n');
                }
                help_view.set_label(&out);
            }
            Err(_) => (),
        }
        Continue(true)
    });

    gtk::main();
}
