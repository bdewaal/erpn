pub use gdk::keys::constants as keys;
pub use gdk::keys::Key;

pub enum MessageToStack {
    SendKey(Key, bool, bool), // key, ctrl, alt
    SendEntry(String),
}

pub enum MessageToGui {
    ShowError(String),
    ClearError,
    ShowStack(Vec<String>),
    ShowHelp(Vec<String>),
}
