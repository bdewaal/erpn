use stackmanager::CalculatorFunction;
use stackobject::StackObject;

pub struct UndoObject {
    pub removed_items: Vec<StackObject>,
    pub added_items_count: u8,
    pub action: CalculatorFunction,
}

impl UndoObject {
    pub fn new(
        removed_items: Vec<StackObject>,
        added_items_count: u8,
        action: CalculatorFunction,
    ) -> Self {
        Self {
            removed_items,
            added_items_count,
            action,
        }
    }
}
